# Extinction Curse maps remake

This FoundryVTT module is compilation of maps for Pathfinder2E Extinction Curse Adventure Path. It includes 74 remade from scratch maps. 
All maps are optimized for [FoundryVTT](https://foundryvtt.com/) - they have wall, doors, windows and lighting set up from the box.

Official maps are way too blurry to use with VTT - this project provides convinient high-quality alternative.

Module includes no official AP material and it's impossible to play AP with it. 
If you want to play this amazing adventure you need to [buy official Paizo books](https://paizo.com/store/pathfinder/adventures/adventurePath/extinctioncurse).

Module fully covers Adventure Path from level 1 to level 20. 

# Maps creators

This module wouldn't be possible without amazing map creators. 

**Lios** ([Patreon](https://www.patreon.com/liospc)) made maps for first book.

**Robster** made most maps for books 2-4 and some maps for book 1 and 5. 

**Luebbi** made several maps for book 3,4,5. 

**Kalnix** ([Github with other modules](https://github.com/Kalnix)) made 9 maps for book 6 and one map for book 5.

**Cyber Commander** made 5 maps for book 6 and 2 maps for book 5. 

**Silver Raven** made one map for book 5.

Creator names are in map names. All maps without "by xxx" in their name are made by mapmaker in the folder title. 

# Installation

Go for Foundry Add-on modules, press "Install module" and search this module by name. Enable module in world settings and maps will appear in compendiums. Alternatively use this link:

```
https://gitlab.com/Ustin/ec-maps-remake/-/raw/master/module.json
```

![Compendium showcase](readme_pics/Compendium.webp)


# Contrubution

If you made additional redrawn EC maps and want to share them with the community - please contact me via Discord. I'll gladly add your maps and expand this module for everyone to enjoy. 
