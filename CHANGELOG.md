# 6.0.0 to 6.5.1

* Added rest of the maps for book 5 and all of the maps for book 6 made by Kalnix, Cyber Commander, Robster and Silver Raven. Module now covers full AP. 
* Compatibility updated to V11
* Changed compendium structure for one compendium with sub-folders.
* Fixed numerous long-standing bugs. 

# 4.2.0

* Updated compatibility for v9
* Added attribution for ruins map in book 5
* Added missing door to hermitage

# 4.1.0

* Included chapter in map names.
* Transfer of stuff from pdf2foundry maps via macro should now be possible.
# 4.0.0

* Added 15 maps covering book 4 and start of book 5 from Robster
* Added 4 maps for books 3-5 from Luebbi, covering some missing maps
* Removed chapter number from map names
# 3.0.0

* Added 14 maps covering book 2 and most of book 3 thanks to contribution from Robster

# 2.0.0 

* Added big map for first chapter of Extinction Curse 2 - Legacy of the lost God
* With switch to .webp module size reduced from 150 MB to 30 MB
* Reworked walls and lighting on some maps to improve perfomance
* Fixed a lot of bugs in first book maps
* Module should now available via integrated module browser

# 1.1.0

* Added Big Top map - thanks Robster!

# 1.0.0

* Initial release

